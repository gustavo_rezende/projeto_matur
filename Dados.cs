﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aplicacao_Matur_json
{
    public class Dados
    {
        public int CODIGOUSUARIO { get; set; }
        public int CODIGOEMPRESA { get; set; }
        public int CODIGOATIVIDADE { get; set; }
        public string DATAUSO { get; set; }
        public int TEMPOUSO { get; set; }

    }
}
