﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Linq;

namespace Aplicacao_Matur_json
{
    class Program
    {
        static void Main(string[] args)
        {
            
            
            var json = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Dados.json");

            var js = new DataContractJsonSerializer(typeof(List<Dados>));
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            var dados = (List<Dados>)js.ReadObject(ms);

            var queryAgrupamento =
                from resultado in dados
                group resultado by new { resultado.CODIGOUSUARIO, 
                                         resultado.DATAUSO, 
                                         resultado.CODIGOEMPRESA, 
                                         resultado.TEMPOUSO } into grupo
                orderby grupo.Key.CODIGOUSUARIO
                select grupo;            

            foreach (var dataMes in queryAgrupamento)
            {
                Console.WriteLine($"Data: {dataMes.Key.DATAUSO}");
                Console.WriteLine($"Código Usuário: {dataMes.Key.CODIGOUSUARIO}");
                Console.WriteLine($"Tempo Gasto: {dataMes.Key.TEMPOUSO}");

                foreach (var empresa in dataMes)
                {                    
                    Console.WriteLine($"\t Código Empresa: {empresa.CODIGOEMPRESA}");
                    Console.WriteLine($"\t Tempo Gasto: {empresa.TEMPOUSO}");

                    foreach(var atividade in dataMes)
                    {
                        Console.WriteLine($"\t\t Código Atividade: {atividade.CODIGOATIVIDADE}");
                        Console.WriteLine($"\t\t Tempo Gasto: {atividade.TEMPOUSO}");
                    }
                }
            }
            
            Console.ReadKey();



            
        }
    }
}
